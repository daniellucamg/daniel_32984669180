import { Component, ViewChildren, QueryList } from '@angular/core';

import { MatTableDataSourceCustomSort } from './extra/mat-table-data-source-custom-sort';
import { CompanyService } from './services/company.service';
import { StockService } from './services/stock.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  //ViewChinldren
  @ViewChildren(MatPaginator) paginator: QueryList<MatPaginator>;
  @ViewChildren(MatSort) sort: QueryList<MatSort>;

  displayedColumns: string[] = ['name', 'segment', 'deviation'];
  dataSource: any = new MatTableDataSourceCustomSort([]);

  constructor(
    private _companyService: CompanyService,
    private _stockService: StockService
  ) { }

  public ngAfterViewInit(): void {
    this.paginator.changes.subscribe((comps: QueryList<MatPaginator>) => {
      this.dataSource.paginator = comps.first;
    });

    this.sort.changes.subscribe((comps: QueryList<MatSort>) => {
      this.dataSource.sort = comps.first;
    });
  }

  public getCompanies(){
      this._companyService.getCompany().subscribe(data => {
        if (data.ok) {
          let rows = [];
          data.body.forEach(row => {
            this._companyService.getAvgById(row.id).subscribe(data => {
              if (data.ok) {
                 let dto = {
                  name: row.name,
                  segment: row.segment,
                  deviation: data.body
                }
                rows.push(dto)
              }
              this.dataSource = new MatTableDataSourceCustomSort(rows);
            })
          })
        }
      })
  }
}
