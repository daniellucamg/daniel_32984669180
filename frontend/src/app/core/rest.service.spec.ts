import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestService } from './rest.service';

describe('RestComponent', () => {
  let component: RestService;
  let fixture: ComponentFixture<RestService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
