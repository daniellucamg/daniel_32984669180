import { Component, OnInit, Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map, timeout } from 'rxjs/operators';

interface IRequestOptions {
  body?: any;
  headers?: HttpHeaders | { [header: string]: string | Array<string> };
  withCredentials?: any;
  observe?: any;
}

@Injectable({
  providedIn: 'root'
})

export class RestService {

  private endpoint = environment.apiUrl;
  
  private options: IRequestOptions = {
    headers: new HttpHeaders(
    { "Content-Type": "application/json", timeout: `${300000}`}),
    withCredentials: true,
    observe: "response"
  }

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(error);
    };
  }

  get(url): Observable<any> {
    return this.http.get(this.endpoint + url, {
        observe: 'response'
      }).pipe(
      catchError(this.handleError<any>('get')),
      map(this.extractData)
      );
  }
  
  postData(url, data): Observable<any> {
    return this.http.post<any>(this.endpoint + url, data).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('post'))
    );;
  }

  post(url, data): Observable<any> {
    return this.http.post<any>(this.endpoint + url, JSON.stringify(data), this.options).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('post'))
    );
  }

  put(url, data): Observable<any> {
    return this.http.put<any>(this.endpoint + url, JSON.stringify(data), this.options).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('put'))
    );
  }

  delete(url, id: number): Observable<any> {
    return this.http.delete<any>(this.endpoint + url + '/' + id, this.options).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('delete'))
    );
  }

}

export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class TimeoutInterceptor implements HttpInterceptor {
  constructor(@Inject(DEFAULT_TIMEOUT) protected defaultTimeout: number) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const timeoutValue = req.headers.get('timeout') || this.defaultTimeout;
    const timeoutValueNumeric = Number(timeoutValue);

    return next.handle(req).pipe(timeout(timeoutValueNumeric));
  }
}

