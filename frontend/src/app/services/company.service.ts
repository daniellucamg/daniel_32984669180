import { Injectable } from '@angular/core';
import { RestService } from '../core/rest.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private _rest: RestService) { }

  getCompany() {
    return this._rest.get('/company');
  }

  getAvgById(id: number) {
    return this._rest.get('/company/companiesAvg/'+id);
  }
}
