import { Injectable } from '@angular/core';
import { RestService } from '../core/rest.service';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private _rest: RestService) { }

  getAll() {
    return this._rest.get('/stock');
  }

  getById(id: number) {
    return this._rest.get('/stock/'+id);
  }
}
