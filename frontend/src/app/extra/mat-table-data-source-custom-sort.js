"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var table_1 = require("@angular/material/table");
var MatTableDataSourceCustomSort = /** @class */ (function (_super) {
    __extends(MatTableDataSourceCustomSort, _super);
    function MatTableDataSourceCustomSort() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._compareFn = new Intl.Collator('pl', { sensitivity: 'base', numeric: true }).compare;
        _this.sortData = function (data, sort) {
            var active = sort.active;
            var direction = sort.direction;
            if (!active || direction == '') {
                return data;
            }
            return data.sort(function (a, b) {
                var valueA = _this.sortingDataAccessor(a, active);
                var valueB = _this.sortingDataAccessor(b, active);
                var comparatorResult = _this._compareFn(valueA, valueB);
                return comparatorResult * (direction == 'asc' ? 1 : -1);
            });
        };
        return _this;
    }
    return MatTableDataSourceCustomSort;
}(table_1.MatTableDataSource));
exports.MatTableDataSourceCustomSort = MatTableDataSourceCustomSort;
//# sourceMappingURL=mat-table-data-source-custom-sort.js.map