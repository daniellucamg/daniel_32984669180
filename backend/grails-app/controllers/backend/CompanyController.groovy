package backend

import grails.converters.JSON
import groovy.time.TimeCategory

class CompanyController {

    def index(){
        List<Company> comp = Company.findAll()
        comp.each {
            c->
                c.stocks.sort(){
                    s->
                        s.priceDate
                }
        }
        render comp as JSON
    }

    def show(Long id) {
        List<Company> comp = Company.findAllById(id)
        comp.each {
            c->
                c.stocks.sort(){
                    s->
                        s.priceDate
                }
        }
        render comp as JSON
    }

    def companiesAvg(Integer id) {
        Company comp = Company.findById(id)
        Float average = 0, deviation  = 0
        if(comp) {
            comp.stocks.each {
                s ->
                    average += s.price
            }
            average /= comp.stocks.size()
            comp.stocks.each {
                s ->
                    deviation += Math.pow((s.price - average), 2)
            }
            deviation = Math.sqrt(deviation / comp.stocks.size())
        }
        render deviation
    }


    def getStocks(String name, Integer hours) {
        long now = System.currentTimeMillis()
        if(name && hours) {
            Company comp
            use(TimeCategory) {
                Date ultimashoras = new Date() - hours.hours
                comp = Company.findByName(name)
                if (comp) {
                    comp.stocks = comp.stocks.findAll() {
                        s ->
                            s.priceDate > ultimashoras
                    }.sort(){
                        s->
                            s.priceDate
                    }
                    println("---------- TASK 2 ----------")
                    println("The number of quotes retrieved: " + comp.stocks.size())
                    println("The total time to be executed: " + (System.currentTimeMillis() - now) + "ms")
                    println("Each stock quote: ")
                    println(comp.stocks.toString())
                    render comp.stocks as JSON
                } else
                    render "Company not exist"
            }
        }
        else {
            render "Returning conference talks..."
        }

    }
}
