package backend

import groovy.time.TimeCategory

class BootStrap {

    List<Stock> stocks1
    List<Stock> stocks2
    List<Stock> stocks3

    def init = { servletContext ->
        stocks1 = new ArrayList<>()
        stocks2 = new ArrayList<>()
        stocks3 = new ArrayList<>()
        use(TimeCategory) {
            Date time
            Random r = new Random();
            int dayStart = 10
            int dayend = 18
            Float price
            for (int i=0; i<43200; i++) {
                time = new Date()-i.minute
                if( time.hours>=dayStart && time.hours<dayend){
                    price =  r.nextInt(50)+r.nextFloat()
                    stocks1.add(new Stock(price: price, priceDate: time))
                    price =  r.nextInt(100)+r.nextFloat()
                    stocks2.add(new Stock(price: price, priceDate: time))
                    price =  r.nextInt(10)+r.nextFloat()
                    stocks3.add(new Stock(price: price, priceDate: time))
                }
            }
        }

        Company company1 = new Company(name: "FORD", segment: "Vehicles")
        stocks1.each {
            s->
                company1.addToStocks(s)
        }

        Company company2 = new Company(name: "AMERICANAS", segment: "Retail")
        stocks2.each {
            s->
                company2.addToStocks(s)
        }

        Company company3 = new Company(name: "GOL", segment: "Aircraft")
        stocks3.each {
            s->
                company3.addToStocks(s)
        }

        company3.save()
        company1.save()
        company2.save()
    }
    def destroy = {
    }
}

