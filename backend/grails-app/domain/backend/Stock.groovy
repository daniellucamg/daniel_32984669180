package backend

import grails.rest.*

@Resource(uri = "/stock", formats=['json'])
class Stock {
    Float price
    Date priceDate
    
    static belongsTo = [company: Company]
    static constraints = {
        price scale: 2
        priceDate nullable: false
    }

    static mapping = {
        sort "priceDate"
    }
}
