package backend

import grails.rest.Resource

@Resource(uri = "/company", formats=['json'])
class Company {
    String name
    String segment

    static hasMany = [stocks : Stock]
    static constraints = {
        name nullable: false, blank: false, unique: true
        segment nullable: false, blank: false
    }

    static mapping = {
        sort "name"
    }
}
